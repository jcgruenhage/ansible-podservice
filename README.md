ansible-podservice
=========

Deploy a container using systemd and podman.

Requirements
------------

This currently only works on Fedora, contributions making this generic are appreciated.

Role Variables
--------------

There are two variables you ***need*** to specify:
 - **podservice_identifier**: The identifier of this container. It will be used
 as the name when running the container, for creating directories and the unit file.
 You should keep this identifier unique.
 - **podservice_container_image_tag**: This is the image that you want to run.

There are a few more variables you usually want to specify, but they aren't needed
strictly speaking:
 - **podservice_container_env**: This is a dictionary containing env vars. Example:
 ```yaml
 podservice_container_env:
     SOME_VAR: "value"
 ```
 - **podservice_container_vols**: This is a dictionary containing volume mappings.
 Example:
 ```yaml
 podservice_container_vols:
     "/path/on/host": "/path/in/container"
 ```
 - **podservice_container_ports**: This is a dictionary containing port mappings.
 Example for mapping container port 80 to IPv6 localhost on the host:
 ```yaml
 podservice_container_ports:
     "::1:8080": "80"
 ```
 - **podservice_container_net**: If you don't want your container on
 the default bridge net but on the host network for example, you can do this:
 ```yaml
 podservice_container_net: host
 ```

Example Playbook
----------------

TODO

License
-------

AGPLv3+
